#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 15:08:50 2019

@author: carl
"""

# e^(t-y)
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

smbl=['1','.','x','1','2','3','4','_','^','>','<','|']
clr=['r','b','g','m','orange','coral','limegreen',
     'purple', 'brown', 'tan', 'gold', 'grey']

# define function for the ode
dydt=lambda y,t: np.exp(t-y)


# initial condition
y0= 1.0

# integration range
t0 = 0.0           # initial time
tn = 1     # final time
t  = np.linspace(t0,tn,num=50)   # using num=50, default

# delta t's
dt = np.asarray([0.5 , 0.1, .05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt



# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * np.exp(t[k]-y[k])
        
    # plot solution for current dt
    plt.figure(1)
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))

# solve the ODE using odeint
ysol = odeint(dydt,y0,t)

# analytical solution: y = sin(t)
ta = np.linspace(t0,tn,num=20) # time for analytical solution
ya = np.e**(ta-y0)             # position

# plot analytical solution
plt.figure(1)
plt.plot(ta,ya,'k',label='Analytical solution')
# plot numerical solution
plt.plot(t,ysol,'.r',label='ODEINT',markersize=7)

plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=e^(t-y) $t$',fontsize=18)

plt.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
plt.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})

plt.show()








# t^2(sin(2t)-2ty)
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# define function for the ode
dydt=lambda y,t: (t**2)*np.sin(2*t)*(2*t*y)

# initial condition
y0= 2.0

# integration range
t0 = 1.0           # initial time
tn = 2.0    # final time
t  = np.linspace(t0,tn,num=10)   # using num=50, default

# delta t's
dt = np.asarray([0.5 , 0.1, .05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt



# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (t[k]**2)*(np.sin(2*t[k])-2*t[k]*y[k])
        
    # plot solution for current dt
    plt.figure(2)
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))

# solve the ODE using odeint
ysol = odeint(dydt,y0,t)

# analytical solution:
tb = np.linspace(t0,tn,num=100) # time for analytical solution
yb = (-1/2*(tb**4)*y0)+(np.cos(2*tb)) - (1/2*(tb**2)*(np.cos(2*tb))) + (1/2*(tb**2)) + (1/2*tb*np.sin(2*tb))             # position

# plot analytical solution
plt.figure(2)
plt.plot(tb,yb,'k',label='Analytical solution')
# plot numerical solution
plt.plot(t,ysol,'.r',label='ODEINT',markersize=7)

plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=t^2(sin(2t)-2ty) $t$',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})

plt.show()


#-y+ty^(1/2)
import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# define function for the ode

dydt=lambda y,t: -y + (t * y**(.5)) 

# initial condition
y0= 2.0

# integration range
t0 = 2.0           # initial time
tn = 3.0   # final time
t  = np.linspace(t0,tn,num=10)   # using num=50, default

# delta t's
dt = np.asarray([0.5 , 0.1, .05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt



# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (-y[k]+(t[k])*(y[k]**.5))
        
    # plot solution for current dt
    plt.figure(3)
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))

# solve the ODE using odeint
ysol = odeint(dydt,y0,t)

# analytical solution:
tc = np.linspace(t0,tn,num=100) # time for analytical solution
yc = -y0*tc + ((1/2*tc**2)*(y0**1/2))                 # position



# plot analytical solution
plt.figure(3)
plt.plot(tc,yc,'k',label='Analytical solution')
# plot numerical solution
plt.plot(t,ysol,'.r',label='ODEINT',markersize=6)

plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=-y+ty^(1/2) $t$',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})

plt.show()


#(ty+y)/(ty+t)

import numpy as np
from scipy.integrate import odeint
import matplotlib.pyplot as plt

# define function for the ode
dydt=lambda y,t: (t*y+y)/(t*y+y) 

# initial condition
y0= 4.0

# integration range
t0 = 2.0         # initial time
tn = 4.0     # final time
t  = np.linspace(t0,tn,num=10)   # using num=50, default

# delta t's
dt = np.asarray([0.5 , 0.1, .05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt



# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * ((t[k]*y[k])+y[k])/((t[k]*y[k])+t[k])
        
    # plot solution for current dt
    plt.figure(4)
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))

# solve the ODE using odeint
ysol = odeint(dydt,y0,t)

# analytical solution: 
td = np.linspace(t0,tn,num=100) # time for analytical solution
yd = (y0/(y0+1))*(td + np.log(td))                # position


# plot analytical solution
plt.figure(4)
plt.plot(td,yd,'k',label='Analytical solution')
# plot numerical solution
plt.plot(t,ysol,'.r',label='ODEINT',markersize=7)

plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=(ty+y)/(ty+t) $t$',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})

plt.show()
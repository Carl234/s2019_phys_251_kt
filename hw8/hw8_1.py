#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 10:36:28 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt

# symbols and colors for plot
smbl=['1','.','x','1','2','3','4','_','^','>','<','|']
clr=['r','b','g','m','orange','coral','limegreen',
     'purple', 'brown', 'tan', 'gold', 'grey']

# initial condition
y0=0.0  

# integration range
t0 = 0.0           # initial time
tn = 1     # final time

# analytical solution
ta = np.linspace(t0,tn,num=100) # time for analytical solution
ya = (-1/9*np.exp(3*ta)) + 1/3*ta*np.exp(3*ta) - 2*y0*ta            # positio
dt = np.asarray([.05,0.1,.05,.01])
ndt=len(dt)                        # number of elements in arra dt


# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (t[k]*np.exp(3.0*t[k])-2.0*y[k])
        
        
        
    # plot solution for current dt
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))
    
from scipy.integrate import odeint
f1=lambda y,t:t*np.exp(3.0*t)-2.0*y
ysol=odeint(f1,y0,ta)

# plot analytical solution
plt.plot(ta,ya,'k',label='Analytical solution')
plt.plot(ta,ysol,'r',label='ODEINT')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=te^3t - 2y',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})


plt.show()


y0=1

# integration range
t0 = 2            # initial time
tn = 3     # final time

# analytical solution
ta = np.linspace(t0,tn,num=100) # time for analytical solution
ya = ta + ((ta)**3/3) - ((ta)**2/2)*y0 + (y0)**2
dt = np.asarray([.05,0.1,.05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt


# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * 1 + (t[k]-y[k])**2
        
    # plot solution for current dt
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))
    

# plot analytical solution
plt.plot(ta,ya,'k',label='Analytical solution')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=1 + (t-y)^2',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})


plt.show()



y0=1  

# integration range
t0 = 1           # initial time
tn = 2  # final time

# analytical solution
ta = np.linspace(t0,tn,num=100) # time for analytical solution
ya = ta + np.log(ta)*y0                # position

# delta t's
dt = np.asarray([.05,0.1,.05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt


# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (1 + (y[k]/t[k]))
        
    # plot solution for current dt
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))
    

# plot analytical solution
plt.plot(ta,ya,'k',label='Analytical solution')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$= 1 + y/t',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})


plt.show()




y0=0.0  

# integration range
t0 = 0.0           # initial time
tn = 2*np.pi   # final time

# analytical solution
ta = np.linspace(t0,tn,num=100) # time for analytical solution
ya = np.sin(ta)                 # position

# delta t's
dt = np.asarray([.05,0.1,.05,.01]) # array dt
ndt=len(dt)                        # number of elements in arra dt


# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition

    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (np.cos(2*t[k]) + np.sin(2*t[k]))
        
    # plot solution for current dt
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))
    

# plot analytical solution
plt.plot(ta,ya,'k',label='Analytical solution')
plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)
plt.title(r'Analytical and Numerical Solutions of $dy/dt$=cos $t$',fontsize=18)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})


plt.show()
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 15:15:05 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt

# symbols and colors for plot
smbl=['1','.','x','1','2','3','4','_','^','>','<','|']
clr=['r','b','g','m','orange','coral','limegreen',
     'purple', 'brown', 'tan', 'gold', 'grey']

g=9.8
# initial condition
y0=1000.0  

# integration range
t0 = 0.0           # initial time
tn = 10     # final time

  # positio
dt = np.asarray([.01,.1,1.0])
ndt=len(dt)                        # number of elements in arra dt


# loop over the dt's
for idt in range(ndt):
    
    # number of elements in the array time for the current dt
    nt = int((tn-t0)/dt[idt])+1

    # create an array with time
    t  = np.linspace(t0,tn,num=nt)
    
    # create an array to store y
    y  = np.zeros(nt)

    # apply Initial Condition
    y[0] = y0
    
    # loop over the Forward Euler algorithm
    for k in range(nt-1):
        y[k+1] = y[k] + dt[idt] * (-g*t[k])
        
        
        
    # plot solution for current dt
    plt.plot(t,y,smbl[idt],color=clr[idt],
             label='$\Delta t$={}'.format(dt[idt]))
    


plt.xlabel(r'$t$',fontsize=20)
plt.ylabel(r'$y$',fontsize=20)

ax=plt.gca()
ax.minorticks_on()                               # activate the minor ticks
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis = 'both', which = 'major', labelsize = 16)
ax.tick_params(axis='x',which='minor',bottom=True)
ax.tick_params(axis='y',which='minor',left=True)
ax.grid(which='major',axis='both',linestyle='-' ,alpha=0.5)
ax.grid(which='minor',axis='both',linestyle='--',alpha=0.2)
plt.legend(prop={'size':14})


plt.show()
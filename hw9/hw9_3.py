#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May  2 19:30:58 2019

@author: carl
"""

import numpy as np
from numpy import sin,cos
import matplotlib.pyplot as plt

# symbols and colors for plot
smbl=['.','.','x','1','2','3','4','_','^','>','<','|']
clr=['k','b','g','m','r','orange','coral','limegreen',
     'purple', 'brown', 'tan', 'gold', 'grey']

#==============================================================================
# FUNCTIONS
#------------------------------------------------------------------------------
# Function to calculate:
# exact solution for the harmonic motion
def fc_harmonic_exact(t,w,x0,v0):
    x= x0   * cos(w*t) + v0 / w * sin(w*t)
    v=-w*x0 * sin(w*t) - v0     * cos(w*t)
    return x,v

#==============================================================================
# MAIN SCRIPT
#------------------------------------------------------------------------------
m   = 1.0           # [kg]  mass
ks  = 1.0           # [N/m] spring constant
w   = np.sqrt(ks/m) # [1/s] 

dt = np.asarray([0.01,0.1,1.0]) # [s]    increments in time

x0 = 1.0     # [m]    initial position
v0 = 2.0     # [m/s]  initial velocity
L=10
t0 =  0.0    # [s]    initial time
tn = 15.0    # [s]    final time
ntmax = 200


#--------------------------------------------------------------------
# numerical solution
# loop over dt's
nt=len(dt)
for idt in range(nt):
    
    nt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    x  = np.zeros(nt)                    # position array
    v  = np.zeros(nt)   
                      # velocity array

    # apply Initial Conditions
    x[0]=x0
    v[0]=v0

    #--------------------------------------------------------------
    # loop to solve ODE using Forward Euler
    for k in range(nt-1):
        x[k+1] = x[k] + dt[idt] * v[k]
        v[k+1] = v[k] + dt[idt] * (ks*(L-x[k]))

    # plot position 
    plt.figure(1)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$x$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,x ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


    # plot velocity 
    plt.figure(2)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$v$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,v ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


# complete figure 1 - Position vs Time
plt.figure(1)
plt.xlabel(r'$t[s]$',fontsize=20)
plt.ylabel(r'$y[m]$',fontsize=20)
plt.title(r'Harmonic Motion: $x$ vs $t$',fontsize=20)

plt.legend(prop={'size':14})


# complete figure 2 - Velocity vs Time
plt.figure(2)
plt.xlabel(r'$t[s]$',fontsize=20)
plt.ylabel(r'$v[m/s]$',fontsize=20)
plt.title(r'Harmonic Motion: $v$ vs $t$',fontsize=20)

plt.legend(prop={'size':14})

T=(.5*m*v**2)
V=(.5*k*x**2)

def totalenergy(T,V):
    E=T+V
    
    return E

edt=np.asarray([.01,.1,1.0])
endt=len(dt)
e0=2

for iedt in range(endt):
    endt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    e  = np.zeros(nt)
    
    e[0]=e0
    
    for k in range(endt-1):
        e[k+1]=T[k]+V[k]
        
    plt.figure(3)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$e$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,e)


#Euler-Cromer
nt=len(dt)
for idt in range(nt):
    
    nt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    x  = np.zeros(nt)                    # position array
    v  = np.zeros(nt)                    # velocity array

    # apply Initial Conditions
    x[0]=x0
    v[0]=v0

    #--------------------------------------------------------------
    # loop to solve ODE using Euler-Cromer
    for k in range(nt-1):
        v[k+1] = v[k] + dt[idt] * (ks*(L-x[k]))
        x[k+1] = x[k] + dt[idt] * v[k+1]

    # plot position 
    plt.figure(4)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$x$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,x ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


    # plot velocity 
    plt.figure(5)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$v$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,v ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))

T1=(.5*m*v**2)
V1=(.5*k*x**2)

def totalenergy1(T1,V1):
    E1=T1+V1
    
    return E1

edt=np.asarray([.01,.1,1.0])
endt=len(edt)
e0=2

for iedt in range(endt):
    endt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    e  = np.zeros(nt)
    
    e[0]=e0
    
    for k in range(endt-1):
        e[k+1]=T1[k]+V1[k]
        
    plt.figure(6)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$y$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})

    plt.plot(t,e)


#verlet
nt=len(dt)
for idt in range(nt):
    
    nt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    x  = np.zeros(nt)                    # position array
    v  = np.zeros(nt)                    # velocity array

    # apply Initial Conditions
    x[0]=x0
    v[0]=v0

    #--------------------------------------------------------------
    # loop to solve ODE using verlet
    for k in range(nt-1):
        x[k+1] = 2*x[k] -x[k-1]+((dt[idt])**2)*(ks*(L-x[k]))
        v[k+1] = (x[k+1]-x[k-1])/(2*dt[idt])

    # plot position 
    plt.figure(7)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$x$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,x ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


    # plot velocity 
    plt.figure(8)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$v$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,v ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))

T2=(.5*m*v**2)
V2=(.5*k*x**2)

def totalenergy2(T2,V2):
    E2=T2+V2
    
    return E2

edt=np.asarray([.01,.1,1.0])
endt=len(dt)
e0=2

for iedt in range(endt):
    endt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    e  = np.zeros(nt)
    
    e[0]=e0
    
    for k in range(endt-1):
        e[k+1]=T2[k]+V2[k]
        
    plt.figure(9)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$e$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,e)



#velocity-verlet

nt=len(dt)
for idt in range(nt):
    
    nt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    x  = np.zeros(nt)                    # position array
    v  = np.zeros(nt)                    # velocity array

    # apply Initial Conditions
    x[0]=x0
    v[0]=v0

    #--------------------------------------------------------------
    # loop to solve ODE using velocity-verlet
    for k in range(nt-1):
        x[k+1] = x[k] +(v[k]*dt[idt]) + (.5*(dt[idt])**2)*(ks*(L-x[k]))
        v[k+1] = v[k] + dt[idt] * ((ks*(L-x[k]))+(ks*(L-x[k+1])))/2

    # plot position 
    plt.figure(10)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$x$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,x ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


    # plot velocity 
    plt.figure(11)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$v$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,v ,smbl[idt],color=clr[idt],markersize=3,
             label=r'$\Delta t = {}\,s$'.format(dt[idt]))


T3=(.5*m*v**2)
V3=(.5*k*x**2)

def totalenergy3(T3,V3):
    E3=T3+V3
    
    return E3

edt=np.asarray([.01,.1,1.0])
endt=len(dt)
e0=2

for iedt in range(endt):
    endt = int((tn-t0)/dt[idt])+1          # number of points in time
    t  = np.linspace(t0,tn,num=nt)       # time array
    e  = np.zeros(nt)
    
    e[0]=e0
    
    for k in range(endt-1):
        e[k+1]=T3[k]+V3[k]
        
    plt.figure(11)
    plt.xlabel(r'$t$',fontsize=20)
    plt.ylabel(r'$e$',fontsize=20)
    plt.grid()
    plt.legend(prop={'size':14})
    plt.plot(t,e)
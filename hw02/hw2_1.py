# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 04:02:32 2019
HW#2 problem 1
@author: Carl
"""

n = int(input('Enter n: '))

S=0  

for i in range(0,n):
    S += 1/(2**i)

S1=0
for i in range(2,n):
    S1 += 4/(3**i)

S2=0
for i in range(1,n):
    S2 += ((3/(2**i))-(2/(5**i)))
    
print('S = {}'.format(S))
print('S1 = {}'.format(S1))
print('S2 = {}'.format(S2))

#when n=500, S goes to 2.0 , S1=.66,S2=2.49
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 10 17:16:39 2019

@author: Carl
HW 2
Problem 5
"""
import matplotlib.pyplot as plt
import numpy as np
from math import pi

#y=x**2
x=np.arange(0,10,.5)

y=x**2

plt.plot(x,y)
plt.show()

#y=sin x increments of pi/4
x_1=np.arange(0,4*pi,pi/4)

y_1=np.sin(x_1)

plt.plot(x_1,y_1)
plt.show()

#y=sin x increments of pi 
x_2=np.arange(0,4*pi,pi)

y_2=np.sin(x_2)

plt.plot(x_2,y_2)
plt.show()

#y=re**(i*theta)
theta = np.arange(-8*pi,8*pi)
a=.1

x_3=np.exp(a*theta)*np.cos(theta)
y_3=np.exp(a*theta)*np.sin(theta)

plt.plot(x_3,y_3)
plt.show()
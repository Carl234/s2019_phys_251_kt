#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 09:29:40 2019

@author: carl
"""


import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

dname = r'/Users/carl/Documents/PHYS251/Class data/' 
fname = 'velocity_run018.txt'
dfname = '{0}/{1}'.format(dname,fname)

run018=np.genfromtxt(dfname,skip_header=1)

t=run018[:,0]

v=run018[:,1]

vd=run018[:,2]

n=t.size

t1=np.zeros(n-1)

v1=np.zeros(n-1)

plt.figure(1)
plt.errorbar(t,v,yerr=vd,fmt='dk',capsize=5,ecolor='r',label='velocity vs time')
plt.plot(t,v,'.',label='Velocity(m/s) vs time(s)')
plt.legend(loc='upper left')
plt.grid()

for i in range (0,n-1):#loop gives us the velocity at each time.
    t1[i]=(t[i]+t[i+1])/2
    v1[i]=(v[i]+v[i+1])/2#graph is linear so t values directly relate to velocity
#didnt use the int. formula because of the relation of time and velocity
plt.figure(2)
plt.plot(t1,v1,'.',label='velocity(m/s) vs time(s)')
plt.xlabel('t(s)')
plt.ylabel('velocity(m/s)')
plt.grid(which='major',color='gray')
plt.legend(loc='upper right')
plt.title('velocity vs time')
plt.savefig('hw5_1(2).png',dpi=300)
plt.show()

def curve(x,v0,A):
    y0 = v0+A*t
    return y0

popt,pcov = curve_fit(curve,t,v,sigma = vd)

print(popt)
print(pcov)#gives uncertainty

v0=popt[0]
A=popt[1]
print('linear regression fit')
print('A is {:.8E}'.format(A))
print('v0 is {:.8E}'.format(v0))

v2=v0+A*t#calculate data

plt.figure(3)
plt.plot(t,v,'.',label='velocity(m/s) vs time(s) data')
plt.plot(t,v2,label='velocity vs time calculation')
plt.xlabel('t(m/s)')
plt.ylabel('velocity(m/s)')
plt.grid(which='major', color='gray')
plt.legend(loc='upper right')
plt.savefig('hw5_1(3).png',dpi=300)
plt.show()

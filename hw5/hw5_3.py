#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 09:38:31 2019

@author: carl
"""

import numpy as np
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.pyplot as plt

dname = r'/Users/carl/Documents/PHYS251/Class data/' 
fname = 'data_2D_grid_T.txt'
dfname = '{0}/{1}'.format(dname,fname)
data=np.genfromtxt(dfname,skip_header=13)


x = data[:,0]
y = data[:,1]
z = data[:,2]


xmin = -10.0
ymin = -10.0
xmax =  10.0
ymax =  10.0
dx = 0.5
dy = 0.5

nx = int((xmax - xmin)/dx)+1
ny = int((ymax - ymin)/dy)+1

X = x.reshape((nx,ny))
Y = y.reshape((nx,ny))
Z = z.reshape((nx,ny))

print(X)
print(Y)
print(Z)

fig = plt.figure()
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X, Y, Z, cmap=cm.terrain,linewidth=0, antialiased=False)
plt.show()

x1=np.arange(-10,10,0.5)
y1=np.arange(-10,10,0.5)
n=x1.size

n1=x.size
x3=np.zeros(n1-1)
y3=np.zeros(n1-1)
z2=np.zeros(n1-1)
k=0

for j in range(0,n-1):
    for i in range (0,n-1):
        a = Z[i,j] * (x3[i+1] -x[i]) * (y3[j+1] - y[i])
        b = Z[i,j+1] * (x3[i+1] -x[i]) * (y[j] - y1[j+1])
        c = Z[i+1,j] * (x[i] -x1[i+1]) * (y3[j+1] - y[j])
        d = Z[i+1,j+1] * (x[i] - x1[i+1]) * (y[j] - y1[j+1])
        z2[k]=((a + b + c + d)/(x3[i+1] - x1[i]) * (y3[j+1] - y1[j]))
        x3[k]=(x[i]+x1[i+1])/2
        y3[k]=(y[j]+y1[j+1])/2
        k=k+1

cols = np.unique(x3).shape[0]

X1 = x3.reshape(-1, cols)
Y1 = y3.reshape(-1, cols)
Z1 = z2.reshape(-1, cols)
print(Z1)

plt.figure(2)
plt.xlabel('x')
plt.ylabel('y')
plt.title('surface')
plt.plot(X1,Y1,Z1)
plt.grid(which='major',color='gray')
plt.show()

fig = plt.figure(3)
ax = fig.gca(projection='3d')
surf = ax.plot_surface(X1,Y1,Z1, cmap=cm.terrain,linewidth=0, antialiased=False)
plt.show()

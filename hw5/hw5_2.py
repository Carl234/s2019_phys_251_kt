#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 09:33:53 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt


dname = r'/Users/carl/Documents/PHYS251/Class data/' 
fname = 'fc_thist_00520'
dfname = '{0}/{1}'.format(dname,fname)
data=np.genfromtxt(dfname,skip_header=1)

t = data[:,0]

s = data[:,7]
n=t.size

plt.figure(1)
plt.plot(t,s,'.',label='concentration(PPM) vs time(s)')
plt.xlabel('t(s)')
plt.ylabel('concentration of H2S in air(ppm)')
plt.legend(loc='upper right')
plt.title('concentration(ppm) vs time(s)')
plt.grid()
plt.savefig('hw5_2(1).png',dpi=300)




tm=np.zeros(500)
sm=np.zeros(500)

j=0
ttest=1.0

for i in range(0,n-1):
    t2=t[i+1]
    if t2>ttest:
        t1=t[i]
        s2=s[i+1]
        s1=s[i]
        stest=s1 + (ttest-t1)*(s2-s1)/(t2-t1)
        sm[j]=stest
        tm[j]=ttest
        j=j+1
        ttest+=1
        if j==400:  #stops when t=400
            break

plt.figure(2)
plt.plot(tm,sm,'.',label='concentration(ppm) vs time every second')
plt.xlabel('t(s)')
plt.ylabel('concentraition of H2S in air(ppm)')
plt.grid(which='major',color='gray')
plt.legend(loc='upper right')
plt.title('concentration(ppm) vs time(s)')
plt.savefig('hw5_2(2).png',dpi=300)
plt.show()

ttest=0
j=0

tk=np.zeros(41)
sk=np.zeros(41)

for i in range(0,n-1):
    t2=t[i+1]
    if t2>ttest:
        t1=t[i]
        s2=s[i+1]
        s1=s[i]
        stest=s1 + (ttest - t1)*(s2-s1)
        sk[j]=stest
        tk[j]=ttest
        j=j+1
        ttest+=10
        if j==401: #stops when t=400
            break

plt.figure(2)
plt.plot(t,s,label='concentration(ppm) vs time(s)')
plt.plot(tk,sk,label='concentration(ppm) vs time w/ 10sec intervals')
plt.xlabel('t(s)')
plt.ylabel('concentration of H2S in air(ppm)')
plt.legend(loc='upper right')
plt.title('concentration(ppm) vs time(s)')
plt.grid(which='major',color='gray')
plt.savefig('hw5_2(3).png',dpi=300)
plt.show()

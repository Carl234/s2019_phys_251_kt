# -*- coding: utf-8 -*-
"""
Created on Sun Feb 17 22:20:35 2019

@author: Carl
"""

import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-np.pi,np.pi,100)

def f(x):  #define cosine function
    y = np.cos(2*x)
    return y

yf=f(x)

#plot
plt.plot(x,yf,'-',label='f(x)')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('Cos 2*x')
plt.savefig("fig1.png",dpi=200)
plt.show()

x1=np.linspace(-np.pi,np.pi,100)

def g(x): #define function
    y = (2*(np.cos(x)**2))-1
    return y
gf=g(x)

#plot
plt.plot(x1,gf,'-',label='g(x)')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('2*cos^2 -1')
plt.show()

x2=np.linspace(-np.pi,np.pi,100)

def h(x):  #define function
    y = (1/np.cos(x)) + np.tan(x)
    return y
hf=h(x)


#plot
plt.plot(x2,hf,'-',label='h(x)')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('sec(x) + tan(x)')
plt.show()
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 11:27:22 2019

@author: Delphbiakop
"""
import numpy as np
import matplotlib.pyplot as plt

x=np.linspace(-4,4,100)

def f_1a(x):                  #defining function
    y = np.sin(x)
    return y
y1=f_1a(x)
def f_1b(x):                   #defining function
    y = x**2
    return y

y2=f_1b(x)  #assign to new variable

#Plot
plt.figure(1)
plt.plot(x,y1,'-',label='sin(x)')
plt.plot(x,y2,'-',label='x^2')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('sin(x) & x^2')
plt.savefig("fig1.png",dpi=200)
plt.show()

def f_2a(x):                       #defining function
    y = np.sin(2*x)
    return y
g1=f_2a(x)#assign to new variable
def f_2b(x):                      #defining function
    y = ((x**3)/10) + ((x**2)/10)
    return y
g2=f_2b(x)#assign to new variable

#Plot
plt.figure(2)
plt.plot(x,g1,'-',label='sin(2x)')
plt.plot(x,g2,'-',label='(x**3/10) + (x**2/10)')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('sin(2x) & (x**3/10) + (x**2/10)')
plt.savefig("fig2.png",dpi=200)
plt.show()

x1 = np.linspace(-6,6,100)

def f_3a(x1):                      #defining function
    y = (np.exp(.001*x)) + np.log(x**3)
    return y
h1=f_3a(x1) #assign to new variable

def f_3b(x1):                        #defining function
    y = (.1*(x**3)) + (.1*(x**2)) - 5
    return y
h2=f_3b(x1)  #assign to new variable

#Plot
plt.figure(3)
plt.plot(x1,h1,'-',label='(e^(.001*x)) + ln(x**3)')
plt.plot(x1,h2,'-',label='(.1*(x**3)) + (.1*(x**2)) - 5')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('(e^.001x + ln(x^3)) & (.1(x^3))+(.1(x^2)) -5')
plt.savefig("fig1.png",dpi=200)
plt.show()

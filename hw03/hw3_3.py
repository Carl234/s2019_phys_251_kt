# -*- coding: utf-8 -*-
"""
Created on Wed Feb 20 16:53:47 2019

@author: Carl
HW3 Prob 3
"""
import numpy as np
import matplotlib.pyplot as plt

t=np.linspace(0,7,100)

x_0=0
y_0=0
theta=45
v_0=40

v0_x = v_0*np.cos(theta)
v0_y = v_0*np.sin(theta)

g=9.8

def x(t):    #define x position
    y = x_0 + v0_x*t
    return y
x=x(t)

def y(t):  #define y position
    y = y_0 + v0_y*t - 1/2*g*t**2
    return y
y=y(t)

n=t.size

# loops for horizontal and vertical velocity components
v_x = np.empty(n)
for i in range(n):
    v_x[i] = v0_x
    
plt.figure(3)
plt.plot(t,v_x, label='v_x(t)')
plt.legend(loc='upper left')

v_y = np.empty(n)
for i in range (n):
    v_y[i] = v0_y - g * t[i]

#plots    


plt.figure(1)
plt.plot(t,x,label='x vs t')
plt.plot(t,y,label='y vs t')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('x vs t & y vs t')
plt.savefig("fig1.png",dpi=200)
plt.show()

plt.figure(2)
plt.plot(t,v_x,label='vx vs t')
plt.plot(t,v_y,label='vy vs t')
plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('v_x vs t & v_y vs t')
plt.savefig("fig2.png",dpi=200)
plt.show()

plt.figure(3)
plt.plot(x,y,label='y vs x')

plt.grid()
plt.xlabel('x')
plt.ylabel('y')
plt.legend()
plt.title('y vs x')
plt.savefig("fig3.png",dpi=200)
plt.show()



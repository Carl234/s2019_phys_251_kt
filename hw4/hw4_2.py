#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 03:13:08 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

dname = r'/Users/carl/Documents/PHYS251/Class data/' 
fname = 'sinusoidal_data.csv'
dfname = '{0}/{1}'.format(dname,fname)

sindata = np.genfromtxt(dfname,delimiter=',')

t=sindata[:,0]
v=sindata[:,1]

def Voltage(t,A,a,w):
    Volt=A*np.exp(-a*t)*np.sin(w*t)
    return Volt

popt,pcov=curve_fit(Voltage,t,v,p0=(0.5,125,5e3))

A=popt[0]
a=popt[1]
w=popt[2]

print('A={}'.format(A))
print('a={}'.format(a))
print('w={}'.format(w))

v1=Voltage(t,*popt)
plt.figure(1)
plt.title('Voltage vs Time')
plt.xlabel('time')
plt.ylabel('Voltage')
plt.grid()
plt.plot(t,v,'.',label='Voltage vs time')
plt.plot(t,v1,label='optimized fit')
plt.legend(loc='upper left')
plt.legend(loc='upper right')
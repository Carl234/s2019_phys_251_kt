# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

"""
Created on Tue Feb 27 16:46:38 2018

@author: Carl
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit


def read(dname,fname):
    
    dfname = '{0}/{1}'.format(dname,fname)
    
    data = np.genfromtxt(dfname,skip_header=1)
    return data
dname = r'/Users/carl/Documents/PHYS251/Class data/'
fname = 'dat_10.txt'
data01=read(dname,fname)
t=data01[:,0]
v=data01[:,1]
sum0 = 0
sum1 = 0
sum2 = 0
sum3 = 0
sum4 = 0
n=t.size

for i in range(n):
     sum0 += t[i]*v[i] 
     sum1 += t[i]
     sum2 += v[i]
     sum3 += t[i]**2
     
m=((n*sum0) - (sum1*sum2))/((n*sum3) - (sum1**2))   
b=((sum3*sum2)-(sum0*sum1))/((n*sum3)-(sum1**2))

print('m={}'.format(m))
print('b={}'.format(b))


v_bar= sum2/n

v_hat=np.empty(n)
for i in range(n):
    v_hat[i] = m * t[i] + b
    

S1=0
S2=0

for i in range(n):
    S1 += (v_hat[i] -v[i])**2
    S2 += (v_bar -v[i])**2

R2 = 1 - (S1/S2)

def myline(x,m,b):
    y = m * x + b
    return y

popt,popc=curve_fit(myline,t,v)

a = popt[0]
b = popt[1]

    


print('R2={}'.format(R2))    
plt.figure(1)
plt.plot(t,v,'.',label='velocity vs time')
plt.xlabel('time')
plt.ylabel('velocity')
plt.legend(loc='upper left')
plt.title('Velocity vs time')
plt.grid()
plt.plot(t,v_hat,label='optimized fit')
plt.legend(loc='upper right')

plt.figure(2)
plt.plot(t,v,'.',label='velocity vs time')
plt.plot(t, myline(t,*popt), 'r--',label='optimized fit')
plt.legend(loc='upper left')
plt.legend(loc='upper right')
plt.xlabel('time(s)')
plt.ylabel('velocity(s)')
plt.grid()
plt.show()
    #plots for Data 10
    
    



dname = r'/Users/carl/Documents/PHYS251/Class data/'
fname = 'dat_11.txt'
data01=read(dname,fname)
t=data01[:,0]
v=data01[:,1]
sum0 = 0
sum1 = 0
sum2 = 0
sum3 = 0
sum4 = 0
n=t.size

for i in range(n):
     sum0 += t[i]*v[i] 
     sum1 += t[i]
     sum2 += v[i]
     sum3 += t[i]**2
     
m=((n*sum0) - (sum1*sum2))/((n*sum3) - (sum1**2))   
b=((sum3*sum2)-(sum0*sum1))/((n*sum3)-(sum1**2))

print('m={}'.format(m))
print('b={}'.format(b))


v_bar= sum2/n

v_hat=np.empty(n)
for i in range(n):
    v_hat[i] = m * t[i] + b
    

S1=0
S2=0

for i in range(n):
    S1 += (v_hat[i] -v[i])**2
    S2 += (v_bar -v[i])**2

R2 = 1 - (S1/S2)

def myline(x,m,b):
    y = m * x + b
    return y

popt,popc=curve_fit(myline,t,v)

a = popt[0]
b = popt[1]

    


print('R2={}'.format(R2))    
plt.figure(1)
plt.plot(t,v,'.',label='velocity vs time')
plt.legend(loc='upper left')
plt.xlabel('time(s)')
plt.ylabel('velocity(m/s)')
plt.title('velocity vs time')
plt.grid()
plt.plot(t,v_hat,label='optimized fit')
plt.legend(loc='upper right')

plt.figure(2)
plt.plot(t,v,'.',label='velocity vs time')
plt.plot(t, myline(t,*popt), 'r--',label='optimized fit')
plt.legend(loc='upper left')
plt.legend(loc='upper right')
plt.xlabel('time(s)')
plt.ylabel('velocity(m/s)')
plt.grid()
plt.show()
  #plots for Data 11

dname = r'/Users/carl/Documents/PHYS251/Class data/'
fname = 'dat_12.txt'
data01=read(dname,fname)
t=data01[:,0]
v=data01[:,1]
sum0 = 0
sum1 = 0
sum2 = 0
sum3 = 0
sum4 = 0
n=t.size

for i in range(n):
     sum0 += t[i]*v[i] 
     sum1 += t[i]
     sum2 += v[i]
     sum3 += t[i]**2
     
m=((n*sum0) - (sum1*sum2))/((n*sum3) - (sum1**2))   
b=((sum3*sum2)-(sum0*sum1))/((n*sum3)-(sum1**2))

print('m={}'.format(m))
print('b={}'.format(b))


v_bar= sum2/n

v_hat=np.empty(n)
for i in range(n):
    v_hat[i] = m * t[i] + b
    

S1=0
S2=0

for i in range(n):
    S1 += (v_hat[i] -v[i])**2
    S2 += (v_bar -v[i])**2

R2 = 1 - (S1/S2)

def myline(x,m,b):
    y = m * x + b
    return y

popt,popc=curve_fit(myline,t,v)

a = popt[0]
b = popt[1]

    


print('R2={}'.format(R2))    
plt.figure(1)
plt.plot(t,v,'.',label='velocity vs time')
plt.xlabel('time')
plt.ylabel('velocity')
plt.legend(loc='upper left')
plt.grid()
plt.plot(t,v_hat,label='optimized fit')
plt.legend(loc='upper right')

plt.figure(2)
plt.title('velocity vs time')
plt.plot(t,v,'.',label='velocity vs time')
plt.legend(loc='upper left')
plt.plot(t, myline(t,*popt), 'r--',label='optimized fit')
plt.legend(loc='upper right')
plt.xlabel('time')
plt.ylabel('velocity')
plt.grid()
plt.show()

     #plot for Data12
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  9 09:34:11 2019

@author: carl
"""

from scipy.integrate import quad
import numpy as np
import matplotlib.pyplot as plt

n=int(input("enter n:  "))

def mid_rec(a,b,n):
    dx=(b-a)/float(n)
    xmid=0
    s=0
    for i in range(1,n):
        xmid=a + (i-.5)*dx
        s+=f(xmid)
    return s*dx
        
        
       

   

f=lambda x:x**2
a=0
b=1


ans=mid_rec(a,b,n)

At,err=quad(f,a,b)

A=np.zeros(100)
delta=np.zeros(100)
n=np.linspace(1,100,100)
A_ex=1/3


for i in range(1,101):
    A[i-1]=mid_rec(a,b,i)
    delta[i-1]=abs((A[i-1]-A_ex)/A_ex)


plt.plot(n,delta)

print(At)
print(ans)
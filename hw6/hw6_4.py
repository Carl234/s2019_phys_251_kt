#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 10:02:57 2019

@author: carl
"""

from scipy.integrate import quad
import numpy as np

n=2

def my_trapezoidal(f,n,a,b):
    dx=(b-a)/float(n)
    s=.5*(f(a)+f(b))
    for i in range(1,n,1):
        
        s+=f(a+i*dx)
    return s*dx

def my_simpson(f,n,a,b):
    dx=(b-a)/float(n)
    s=0
    for i in range(n):
        xi=a+i*dx
        xi1=a+(i+1)*dx
        xmid=(xi+xi1)/2
        s+=f(xi)+4*f(xmid)+f(xi1)
    return s*dx/6

f=lambda x:x**3/81+1
a=0
b=4

ans_t=my_trapezoidal(f,n,a,b)
ans_s=my_simpson(f,n,a,b)

At,err=quad(f,a,b)

trap=(10**-2)*(ans_s)+ans_s


print(At)
print(ans_t)
print(ans_s)
print(trap)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 11 09:43:26 2019

@author: carl
"""

from scipy.integrate import quad
import numpy as np

n=int(input("enter n:  "))

def my_simpson(f,n,a,b):
    dx=(b-a)/float(n)
    s=0
    for i in range(n):
        xi=a+i*dx
        xi1=a+(i+1)*dx
        xmid=(xi+xi1)/2
        s+=f(xi)+4*f(xmid)+f(xi1)
    return s*dx/6

f=lambda x:x**4
a=0
b=4

ans=my_simpson(f,n,a,b)

At,err=quad(f,a,b)
print(At)
print(ans)

f1=lambda x:2/(x-4)
a1=0
b1=3
ans1=my_simpson(f1,n,a1,b1)

At1,err=quad(f1,a1,b1)
print(At1)
print(ans1)

f2=lambda x:((x**2)*np.log(x))
a2=1
b2=4

ans2=my_simpson(f2,n,a2,b2)

At2,err=quad(f2,a2,b2)
print(At2)
print(ans2)

f3=lambda x:((np.exp(2*x))*(np.sin(2*x)))
a3=0
b3=2*3.14

ans3=my_simpson(f2,n,a2,b2)

At3,err=quad(f3,a3,b3)
print(At3)
print(ans3)
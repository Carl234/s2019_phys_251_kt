#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 18:10:00 2019

@author: carl
"""
tot=0
for i in range(1000):
    if(i%3==0 or i%5==0):
        tot+=i
print(tot)
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 11:14:40 2019

@author: carl
"""

c=0
a=0
b=1

tot=0
while b<4e6:
    c=a+b
    a=b
    b=c
    
    if(c%2==0):
        tot+=c
        
print('sum of even fibonnacci terms:{}'.format(tot))
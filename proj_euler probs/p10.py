#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 26 11:15:14 2019

@author: carl
"""
import math
 

def Prime(num):
    
    for i in range(2,int(math.sqrt(num))+1):
        if (num % i == 0):
            return False
 
    return True
 
tot_prime = 0
 
for i in range(2,2000000):
    
    if Prime(i):
        tot_prime += i
 
print('sum of primes:{}'.format(tot_prime))
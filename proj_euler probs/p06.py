#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 19:23:34 2019

@author: carl
"""

# sum of the squares
sumSqr = 0
 
# sums
sums = 0
 
# Iterate through numbers [1:100] 1-100 inclusevely
for i in range(1,101):
    sumSqr += i*i
    sums += i
 
# Square the individuals sums to find square of sums
sqrSum = sums * sums
 

print('The sum of the squares is:{}'.format(sumSqr))
print('The square of the sums is:{}'.format(sums))
print('The difference is:{}'.format(sqrSum - sumSqr))
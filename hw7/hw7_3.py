#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr 21 20:36:54 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(-5,5,200)
y = np.empty(x.size)
dfdx = np.empty(x.size)
d2dx = np.empty(x.size)
dx = x[1]-x[0]

for i in range(0,x.size):
    y[i] = np.exp(-x[i]**2)
    dfdx[i] = -2*x[i]*np.exp(-x[i]**2)
    d2dx[i] = 4*(x[i]**2)*np.exp(-x[i]**2)
    
dfdxf = np.empty(x.size)
dfdxb = np.empty(x.size)
dfdxc = np.empty(x.size)
d2dxc = np.empty(x.size)

for i in range(1,x.size-1):
    dfdxf[i] = (y[i+1]-y[i])/dx
    dfdxb[i] = (y[i]-y[i-1])/dx
    dfdxc[i] = (y[i+1]-y[i-1])/(2*dx)
    d2dxc[i] = (y[i+1]-(2*y[i])+y[i-1])/(dx**2)
    
plt.plot(x,y,'.',markersize='2')
plt.plot(x,dfdx,'g.',markersize='2')
plt.plot(x,dfdxf,'y.',markersize='2', label='forward differentiation')
plt.plot(x,dfdxb,'b.',markersize='2', label ='backward differentiation')
plt.plot(x,dfdxc,'r.',markersize='2', label='central differentiation')
plt.plot(x,d2dxc,'k.',markersize='2', label='2nd central differentiation')
plt.legend(loc=9)
plt.grid()
plt.xlabel('x-axis')
plt.ylabel('y-axis')
plt.ylim(-2.0,1.5)

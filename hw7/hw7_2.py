#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 07:21:20 2019

@author: carl
"""


import numpy as np
import matplotlib.pyplot as plt


x = np.linspace(5,30,200)
y = np.empty(x.size)
dfdx = np.empty(x.size)
dx = x[1]-x[0]

for i in range(0,x.size):
    y[i] = np.sin(x[i])/x[i]
    dfdx[i] = ((x[i]*np.cos(x[i]))-np.sin(x[i]))/(x[i]**2)
    
dfdxf = np.empty(x.size)
dfdxb = np.empty(x.size)
dfdxc = np.empty(x.size)

for i in range(0,x.size-1):
    dfdxf[i] = (y[i+1]-y[i])/dx

for i in range(1,x.size-1):
    dfdxb[i] = (y[i]-y[i-1])/dx
    dfdxc[i] = (y[i+1]-y[i-1])/(2*dx)


plt.plot(x,y,'.',markersize='2')
plt.plot(x,dfdx,'g.',markersize='2')
plt.plot(x,dfdxf,'y.',markersize='2', label='forward differentiation')
plt.plot(x,dfdxb,'b.',markersize='2', label ='backward differentiation')
plt.plot(x,dfdxc,'r.',markersize='2', label='central differentiation')
plt.ylim([-0.20,0.20])
plt.legend(loc=9)
plt.grid()
plt.xlabel('x-axis')
plt.ylabel('y-axis')
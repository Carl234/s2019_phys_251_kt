#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr 20 07:16:59 2019

@author: carl
"""

import numpy as np
import matplotlib.pyplot as plt
pi = np.pi

x = np.linspace(0,2*pi,100)
y = np.empty(x.size)
dfdx = np.empty(x.size)
dx = x[1]-x[0]

for i in range(0,x.size):
    y[i] = np.cos(x[i])+np.sin(x[i])
    dfdx[i] = np.cos(x[i])-np.sin(x[i])

plt.plot(x,y,'.',label='given function',markersize=5)
plt.plot(x,dfdx,'y.',label='analytical solution',markersize=5)

dfxf = np.empty(x.size)
dfxb = np.empty(x.size)
dfxc = np.empty(x.size)

for i in range(1,x.size-1):
    dfxf[i] = (y[i+1]-y[i])/dx
    dfxb[i] = (y[i]-y[i-1])/dx
    dfxc[i] = (y[i+1]-y[i-1])/(2*dx)
    
plt.plot(x,dfxf,'r.',label = 'forward difference', markersize=2)
plt.plot(x,dfxb,'b.',label = 'backward difference',markersize=2)
plt.plot(x,dfxc,'g.',label = 'central difference',markersize=2)
plt.legend(loc=9)
plt.grid()
plt.xlabel('x-axis')
plt.ylabel('y-axis')
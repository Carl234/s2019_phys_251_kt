# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 17:32:37 2019

@author: Carl
"""

from math import pi



r=int(input('enter radius:  '))

def sphereinfo(r):
    vol = (4.0/3.0)*pi*r**3
    surface= 4*pi*r**2
    
    return (vol,surface)
    

v,s = sphereinfo(r)
print('volume is {} m^3 and surface_area is {} m^2'.format(v,s))
# -*- coding: utf-8 -*-
"""
Homework 1
Problem 5

Created on Wed Feb  6 13:32:17 2019

@author: Carl
"""

from math import pi

g = 6.67E-11  
m = 5.97E24  #kg
r = 6371     #km


t = int(input('enter time in minutes:  '))

s = t * 60
def height(s):
    h = ((g*m*s**2)/(4*pi))**1/3
    return h

height=height(s)

print('the altitude is {}'.format(height))


#5c 23.93*60=1435.8 24*60=1440
#5c 1440-1435.8=4.2 it will differ by 4.2mins
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  4 08:28:06 2019

@author: carl
"""

from math import pi,e

x = pi + e

print('{0:4.2E}'.format(x))